@extends('layout.master')
@section('title')
<h1>Halaman Table Cast</h1>
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama </th>
        <th scope="col">umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th>{{$key+1}}</th>
            <td>{{$item ->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm"> details </a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm"> edit </a> 
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
          </tr>
            
        @empty
        <tr>
            <td> Data base masih kosong</td>
        </tr>
            
        @endforelse
     
      
    </tbody>
  </table>
@endsection