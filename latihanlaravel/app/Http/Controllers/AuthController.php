<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function bio()
  {
      return view('page.biodata');
  }

  public function welcome(Request $request)
  {
      
      $depan = $request['Fname'];
      $belakang = $request['Lname'];
      return view('page.welcome',compact ('depan', 'belakang'));
      
      //dd ($request ->all());//
  }
}
